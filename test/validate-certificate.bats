#!/usr/bin/env bats

load test_helper
load '../vendor/test-helper/bats-support/load'

bin=${VALIDATE_CERTIFICATE_BIN:-"./validate-certificate"}

## General options tests

setup_file () {
  export TEST_DIR=$(mktemp -d)

  gen_ca_and_cert 1
  gen_ca_and_cert 2
  gen_ca_and_cert 3
}

teardown_file () {
  rm -rf "$TEST_DIR"
}

@test "--help should return the usage" {
    run $bin --help

    [ "$status" -eq 0 ]
    echo "$output" | grep -q "Usage"
}

@test "-h should return the usage" {
    run $bin -h

    [ "$status" -eq 0 ]
    echo "$output" | grep -q "Usage"
}

@test "-V should return the version" {
    run $bin -V

    [ "$status" -eq 0 ]
    echo "$output" | grep -q "v"
}

@test "--version should return version" {
    run $bin --version

    [ "$status" -eq 0 ]
    echo "$output" | grep -q "v"
}

@test "A fake arg should be rejected with an error and print the usage" {
    run $bin --lol-this-is-fake

    [ "$status" -eq 1 ]
    echo "$output" | grep -q "Error"
    echo "$output" | grep -q "Usage"
}

## Tool specific tests

@test "Positional arguments should be rejected with an error and print the usage" {
    run $bin lol

    [ "$status" -eq 1 ]
    echo "$output" | grep -q "Error"
    echo "$output" | grep -q "Usage"
}

@test "-c & -k should be accepted together" {
  $bin -c "${TEST_DIR}/cert-1.crt" -k "${TEST_DIR}/cert-1.key"
}

@test "--certificate & --key should be accepted together" {
  $bin --certificate "${TEST_DIR}/cert-1.crt" --key "${TEST_DIR}/cert-1.key"
}

@test "-c & -C should be accepted together" {
  $bin -c "${TEST_DIR}/cert-1.crt" -C "${TEST_DIR}/ca-1.crt"
}

@test "--certificate & --ca-chain should be accepted together" {
  $bin --certificate "${TEST_DIR}/cert-1.crt" --ca-chain "${TEST_DIR}/ca-1.crt"
}

@test "-c, -C, and -k should be accepted together" {
  $bin -c "${TEST_DIR}/cert-1.crt" -C "${TEST_DIR}/ca-1.crt" -k "${TEST_DIR}/cert-1.key"
}

@test "--certificate, --ca-chain, and --key should be accepted together" {
  $bin --certificate "${TEST_DIR}/cert-1.crt" \
       --ca-chain "${TEST_DIR}/ca-1.crt" \
       --key "${TEST_DIR}/cert-1.key"
}

@test "-C & -k without certificate should be rejected with an error" {
  run $bin -C "${TEST_DIR}/ca-1.crt" \
           -k "${TEST_DIR}/cert-1.key"

  [ "$status" -eq 1 ]
  echo "$output" | grep -q "Error"
  echo "$output" | grep -q "Usage"
}

@test "--ca-chain & --key without certificate should be rejected with an error" {
  run $bin --ca-chain "${TEST_DIR}/ca-1.crt" \
           --key "${TEST_DIR}/cert-1.key"
           
  [ "$status" -eq 1 ]
  echo "$output" | grep -q "Error"
  echo "$output" | grep -q "Usage"
}

@test "-c can't be the only argument" {
  run $bin -c "${TEST_DIR}/cert-1.crt"

  [ "$status" -eq 1 ]
  echo "$output" | grep -q "Error"
  echo "$output" | grep -q "Usage"
}

@test "--certificate can't be the only argument" {
  
  run $bin --certificate "${TEST_DIR}/cert-1.crt"

  [ "$status" -eq 1 ]
  echo "$output" | grep -q "Error"
  echo "$output" | grep -q "Usage"
}

@test "-C can't be the only argument" {
  run $bin -C "${TEST_DIR}/ca-1.crt"

  [ "$status" -eq 1 ]
  echo "$output" | grep -q "Error"
  echo "$output" | grep -q "Usage"
}

@test "--ca-chain can't be the only argument" {
  run $bin --ca-chain "${TEST_DIR}/ca-1.crt"

  [ "$status" -eq 1 ]
  echo "$output" | grep -q "Error"
  echo "$output" | grep -q "Usage"
}

@test "-k can't be the only argument" {
  run $bin -k "${TEST_DIR}/cert-1.key"

  [ "$status" -eq 1 ]
  echo "$output" | grep -q "Error"
  echo "$output" | grep -q "Usage"
}

@test "--key can't be the only argument" {
  run $bin --key "${TEST_DIR}/cert-1.key"

  [ "$status" -eq 1 ]
  echo "$output" | grep -q "Error"
  echo "$output" | grep -q "Usage"
}

# Test the actual logic of the script

@test "key and cert have the same modulus, the script should succeed" {
  $bin --key "${TEST_DIR}/cert-1.key" \
       --certificate "${TEST_DIR}/cert-1.crt"
}

@test "key and cert don't have the same modulus, the script should fails" {
  run $bin --key "${TEST_DIR}/cert-1.key" \
           --certificate "${TEST_DIR}/cert-2.crt"
  
  [ "$status" -eq 1 ]
}

@test "the cert is issue from the right CA, the script should succeed" {
  $bin --certificate "${TEST_DIR}/cert-1.crt" --ca-chain "${TEST_DIR}/ca-1.crt"
}

@test "the cert is not of the CA, the script should fails" {
  run $bin --certificate "${TEST_DIR}/cert-1.crt" --ca-chain "${TEST_DIR}/ca-2.crt"
  
  [ "$status" -eq 1 ]
}

@test "key and cert have the same modulus, and are issue from the correct CA, the script should succeed" {
  $bin --key "${TEST_DIR}/cert-1.key" \
       --certificate "${TEST_DIR}/cert-1.crt" \
       --ca-chain "${TEST_DIR}/ca-1.crt"
}

@test "key and cert have the same modulus, but aren't issue from the correct CA, the script should fails" {
  run $bin --key "${TEST_DIR}/cert-1.key" \
       --certificate "${TEST_DIR}/cert-1.crt" \
       --ca-chain "${TEST_DIR}/ca-2.crt"
       
  [ "$status" -eq 1 ]
}

@test "key and cert don't have the same modulus, but are issue from the correct CA, the script should fails" {
  run $bin --key "${TEST_DIR}/cert-2.key" \
       --certificate "${TEST_DIR}/cert-1.crt" \
       --ca-chain "${TEST_DIR}/ca-1.crt"
 
  [ "$status" -eq 1 ]
}

@test "key and cert don't have the same modulus, and aren't issued from the correct CA, the script should fails" {
  run $bin --key "${TEST_DIR}/cert-2.key" \
       --certificate "${TEST_DIR}/cert-1.crt" \
       --ca-chain "${TEST_DIR}/ca-3.crt"
 
  [ "$status" -eq 1 ]
}