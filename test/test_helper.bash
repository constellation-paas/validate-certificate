gen_ca_and_cert() {
  local number="$1"

  # Not aiming for security here, just speed of tests
  openssl genrsa -out "${TEST_DIR}/ca-${number}.key" 1024
  openssl req -x509 -new -nodes \
    -key "${TEST_DIR}/ca-${number}.key" \
    -sha256 -days 1024 -out "${TEST_DIR}/ca-${number}.crt" \
    -subj "/C=US/ST=CA/O=Constellation/CN=test.constellation"

  openssl genrsa -out "${TEST_DIR}/cert-${number}.key" 1024
  openssl req -new -sha256 -key "${TEST_DIR}/cert-${number}.key" \
    -subj "/C=US/ST=CA/O=Constellation/CN=test-${number}.constellation" \
    -out "${TEST_DIR}/cert-${number}.csr"

  openssl x509 -req -in "${TEST_DIR}/cert-${number}.csr" \
    -CA "${TEST_DIR}/ca-${number}.crt" \
    -CAkey "${TEST_DIR}/ca-${number}.key" \
    -CAcreateserial -out "${TEST_DIR}/cert-${number}.crt" \
    -days 500 -sha256
}
