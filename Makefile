.ONESHELL:
SHELL = bash
.SHELLFLAGS = -ec

.DEFAULT_GOAL := help

bats = ./vendor/bats-core/bin/bats

.PHONY: t, test
## Shortcut to test
t: test
## Run the test
test:
	$(bats) test

.PHONY: help
help: bin/pretty-make
	@$< $(firstword $(MAKEFILE_LIST))

bin/pretty-make:
	@bash <(curl -Ls https://raw.githubusercontent.com/awea/pretty-make/master/scripts/install.sh)