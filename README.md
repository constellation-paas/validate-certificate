# 🔍Validate Certificate

This project provide a simple `validate-certificate` script that ensure that a
leaf certificate, a key have the same modulus (and thus, are compatible
together), and that the provided CA chain can validate the leaf certificate.

## Requirement

The script require:
- `bash(1)`
- `openssl(1)`

## 🔨Usage

The tool is completely silent and will return sucessfully only if all the
arguments provided are compatible together.

Command line options:
- `--certificate` (required): The path to the certificate to check
- `--key`: The path to the key to check
- `--ca-chain`: The path to the the CA chain to check

```
validate-certificate --certificate <cert path> [--key <key path>| --ca-chain <chain path>]
```

### Exemple

Check a certificate and a key:
```
$ validate-certificate --certificate certificate.pem --key key.pem
```

Check that a certificate is correctly validated by a cert chain:
```
$ validate-certificate --certificate certificate.pem --ca-chain chain.pem
```

Check them all:
```
$ validate-certificate --certificate certificate.pem --key key.pem --ca-chain chain.pem
```

## 💯Testing

You can run the test with:
```
$ make test
```

## 🔄Versionning

This project uses SemVer. The public API is defined as the possible interaction with the tool via the shell.

## 📓 Resources

You can find the docs for [bats(1)][bats] (our test framework) here, in the
[ztombol/bats-docs repository][bats-doc]

[bats]: https://github.com/bats-core/bats-core/tree/v1.2.0
[bats-doc]: https://github.com/ztombol/bats-docs

## 📝License

This software is distributed under the GPLv3 license. Please check the [COPYING](COPYING) file.